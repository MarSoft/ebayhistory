#!/usr/bin/env python3

import asyncio
import datetime as dt
import json
from pprint import pprint
import sys

import aiohttp
from tqdm import tqdm

HIST_URL = 'http://www.ebay.com/myb/PurchaseHistoryOrdersContainer'
ENTRY_ARGS = {
    #'ipp': '25',
    'Period': '4',  # year 2015
    #'Filter': '1',  # ?
    #'radioChk': '1',  # show hidden?
    #'GotoPage': '12',  # page number
    #'_trksid': 'p2057872.m2749.l4670',
    #'cmid': '2749',
    #'melid': 'page',
    #'_trksid': 'p2057872.m2749.l4670',
    #'pageId': '2057872',
    #'_': '1486605616263',
}

class App:
    params = {}

    async def init(self, cookie_str):
        cookies = [c.strip() for c in cookie_str.split(';')]
        self.cookies = {
            p[0]: p[2] for p in
            (c.partition('=') for c in cookies)
        }

    async def get_page(self, **params):
        params.update(self.params)
        print(params)
        async with aiohttp.get(
            HIST_URL,
            params=params,
            cookies=self.cookies,
        ) as ret:
            return (await ret.json())['data']

    async def handle_page(self, period, n):
        data = await self.get_page(Period=period, GotoPage=n)
        items = []

        def doPrice(binfo):
            return dict(
                localPrice=binfo['convertedPrice'],
                localShipping=binfo[
                    'convertedShippingPrice'],
                price=binfo['price'],
                shipping=binfo['shippingPrice'],
                shippingType=binfo['shippingType'],
            )
        def doActions(actions):
            return {
                a['label']: a['actionParam']['url']
                for a in item['actions']
                if a['action'] == 'link'
            }
        def doState(item, key):
            data = item['data']['itemInfo']['lineItemStates']
            if not data[key]:
                return None
            ikey = data[key]['key']
            if 'Date' in ikey:
                return doDate(data[key]['params']['date'], '%m/%d/%y')
            elif 'Not' in ikey or ikey.startswith('not'):
                return False
            elif 'feedback' in ikey:
                return True
            elif ikey == 'partiallyRefunded':
                return 'partial'

            print('WARN unknown info', data, ikey)
            return ikey
        def doDate(date, fmt):
            d = dt.datetime.strptime(date, fmt)
            return str(d)

        for item in data['items']:
            self.result(dict(
                id=item['data']['orderInfo']['orderId'],
                seller=item['data']['orderInfo']['sellerInfo']['login'],
                price=doPrice(item['data']['buyingInfo']),
                date=doDate(item['data']['orderInfo']['orderDate'], '%b %d, %Y'),
                items=[
                    dict(
                        id=i['data']['itemInfo']['itemId'],
                        name=i['data']['itemInfo']['actionParam']['label'],
                        url=i['data']['itemInfo']['actionParam']['url'] or
                        i['data']['itemInfo']['actionParam']['fallbackUrl'],
                        urlGen='http://www.ebay.com/itm/%s' %
                        i['data']['itemInfo']['itemId'],
                        price=doPrice(i['data']['buyingInfo']),
                        thumbnail=i['data']['imageInfo']['multiImages'][0],
                        stats={
                            'paid': doState(i, 'fundingStatus'),
                            'shipped': doState(i, 'shipped'),
                            'feedbackLeft': doState(i, 'feedbackLeft'),
                            'feedbackReceived': doState(i, 'feedbackReceived'),
                            'refund': doState(i, 'refundStatus'),
                            'return': doState(i, 'returnStatus'),
                            'inquiry': doState(i, 'inquiryStatus'),
                            'cancel': doState(i, 'cancelStatus'),
                            'display': doState(i, 'displayState'),
                        },
                        note=i['data']['itemInfo']['noteInfo']['note'],
                        quantity=i['data']['itemInfo']['quantity'],
                        transactionId=i['data']['itemInfo']['transactionId'],
                        actions=doActions(i['actions']),
                    )
                    for i in item['data']['items']
                ],
                actions=doActions(item['actions']),
            ))

    def handle_page_and_tick(self, *args):
        try:
            return self.handle_page(*args)
        finally:
            self.ticker.update()

    def result(self, record):
        self._results.append(record)

    async def main(self):
        try:
            with open('cookies.txt', 'r') as f:
                cookies = f.read()
        except FileNotFoundError:
            cookies = input('eBay Cookies: ')
        if cookies[0] == cookies[-1] == '"':
            cookies = cookies[1:-1]

        await self.init(cookies)

        self.params = {}

        queries = []
        for period in (1, 2, 3, 4):
            firstpage = await self.get_page(Period=period)
            queries += [
                (period, int(url.split('GotoPage=')[-1].partition('&')[0]))
                for url in firstpage['pagination']['pages']
            ]
        self.ticker = tqdm(total=len(queries))
        self._results = []
        await asyncio.wait([
            self.handle_page_and_tick(period, n)
            for (period, n) in queries
        ])
        self._results.sort(
            key=lambda r: r['date'],
        )
        json.dump(self._results, sys.stdout)

if __name__ == '__main__':
    app = App()
    asyncio.get_event_loop().run_until_complete(app.main())
