#!/bin/sh
# Convert purchase history json to HTML with images and links, for visual searching by image
# Usage: ./htmlify.sh hist-file.json [grepargs] > output.html
# Example: ./htmlify.sh hist-20170203.json -vE 'lace|beads' > /tmp/out.html

injson=$1
shift
# if grepargs not provided then effectively disable grep
[ -z "$2" ] && set -- .

cat "$injson" \
	| jq -r '.[] | .items | .[] | .thumbnail + " " + .urlGen + " " + .name' \
	| grep "$@" \
	| while read img url name; do
		echo "<a href=\"$url\"><img src=\"$img\" alt=\"$name\"></a>"
	done
